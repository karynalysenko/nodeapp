require("dotenv").config( {path:'./.env' })
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const session = require('express-session')
const { Pool } = require('pg')
// const pgSession = require('connect-pg-simple')(session);

const port = 3000

app.use(express.static('public'))
app.set('view engine', 'ejs')
app.use(bodyParser.json())
//Important 
app.use(bodyParser.urlencoded({ extended: true }))


app.listen(port, () => {
  console.log(`App running on port ${port}`)
});

// app.use(session({
//   store: new pgSession({
//     pool: pool,
//     tableName: 'session'
//   }),
//   secret: 'your-secret-key',
//   resave: false,
//   saveUninitialized: false
// }));

const pool = new Pool({
  host: "localhost",
  port: "5432",
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: "star-wars-quotes"
});

pool.connect()
  .then(() => {
    console.log('Connected to Database');
  })
  .catch((error) => {
    console.error('Error connecting to database:', error);
  });

app.get('/', (req, res) => {
  pool.query('SELECT * FROM public.quotes', (error, result) => {
    if (error) {
      throw error;
    }
    //console.log(result.rows);
    res.render('index.ejs', { quotes: result.rows });
  });
});

app.post('/quotes', (req, res) => {
  const {
    name,
    quote
  } = req.body

  pool.query('INSERT INTO public.quotes (name, quote) VALUES ($1,$2)', [name, quote], (error, result) => {
    if (error) {
      throw error;
    }
    console.log('inserted')
    //res.json(`Inserted new quote`)

    res.redirect('/')
  });
});

app.put('/quotes/:id', (req, res) => {
  const quoteId = req.params.id
  const {name,quote} = req.body
  
  pool.query('UPDATE public.quotes SET name = $1, quote = $2 WHERE id = $3', [name, quote, quoteId], (error, result) => {
    if (error) {
      throw error;
    }
    console.log('updated');
    res.status(200).json();
    //res.redirect('/');
  });  
});

app.delete('/quotes/:id', (req, res) => {
  const quoteId = req.params.id
  console.log('Deleting quote with ID:', quoteId)

  pool.query('DELETE FROM public.quotes WHERE id = $1', [quoteId], (error, result) => {
    if (error) {
      console.error(error)
      res.status(500).json({ message: 'Internal server error' })
    } else {
      console.log('Deleted row')
      res.status(204).send()
    }
  })
})
