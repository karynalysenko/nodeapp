const deleteButtons = document.querySelectorAll('.delete-button')
const editButtons = document.querySelectorAll(".edit-button");
const addQuoteForm = document.querySelector("#add-quote-form");

deleteButtons.forEach(button => {
  button.addEventListener('click', event => {
    const quoteId = event.target.getAttribute('data-quote-id')
    console.log('Button clicked for quote with id:', quoteId)

    fetch(`/quotes/${quoteId}`, {
      method: 'DELETE'
    })
      .then(response => {
        // console.log(response)
        window.location.reload()
      })
      .catch(error => console.error(error))
  })
})

editButtons.forEach(button => {
  button.addEventListener("click", (event) => {
    const id = event.target.dataset.quoteId;
    const name = event.target.dataset.quoteName;
    const quote = event.target.dataset.quoteQuote;

    console.log(id, name, quote);

    const nameInput = document.querySelector("#name");
    const quoteInput = document.querySelector("#quote");

    nameInput.value = name;
    quoteInput.value = quote;

    addQuoteForm.addEventListener("submit", (event) => {
      event.preventDefault();

      const name = document.querySelector("#name").value;
      const quote = document.querySelector("#quote").value;

      fetch(`/quotes/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ name, quote })
      })
      .then(() => {
        nameInput.value = '';
        quoteInput.value = '';
        window.location.reload();
      })
      .catch((error) => {
        console.error("Error:", error);
      });
    });
  });
});
