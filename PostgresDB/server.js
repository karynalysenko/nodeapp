const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const { Pool } = require('pg')
const port = 3000

app.use(express.static('public'))
app.set('view engine', 'ejs')
app.use(bodyParser.json())
//Important 
app.use(bodyParser.urlencoded({ extended: true }))


const pool = new Pool({
  host: "localhost",
  port: "5432",
  user: "postgres",
  password: "0000",
  database: "star-wars-quotes"
})

pool.connect()
  .then(() => {
    console.log('Connected to Database');
  })
  .catch((error) => {
    console.error('Error connecting to database:', error);
  });

app.listen(port, () => {
  console.log(`App running on port ${port}`)
})

// app.get('/', (req, res) => {
//   res.sendFile(__dirname + '/index.html')
// })


app.get('/', (req, res) => {
  pool.query('SELECT * FROM public.quotes', (error, result) => {
    if (error) {
      throw error;
    }
    //console.log(result.rows);
    res.render('index.ejs', { quotes: result.rows });
  });
});

app.post('/quotes', (req, res) => {
  const {
    name,
    quote
  } = req.body
  //console.log("vai inserir")
  //console.log(req.body)

  pool.query('INSERT INTO public.quotes (name, quote) VALUES ($1,$2)', [name, quote], (error, result) => {
    if (error) {
      throw error;
    }
    console.log('inserted')
    //res.json(`Inserted new quote`)

    res.redirect('/')
  });
});

app.put('/quotes', (req, res) => {
  const oldName = 'Yoda'
  const {
    name,
    quote
  } = req.body
  
  pool.query('SELECT * FROM public.quotes WHERE name = $1', [oldName], (error, result) => {
    if (error) {
      throw error;
    }

    if (result.rows.length > 0) {
      pool.query('UPDATE public.quotes SET name = $1, quote = $2 WHERE name = $3', [name, quote, oldName], (error, result) => {
        if (error) {
          throw error;
        }
        console.log('updated');
        res.redirect('/');
      });

    } else {
      pool.query('INSERT INTO public.quotes (name, quote) VALUES ($1, $2)', [name, quote], (error, result) => {
        if (error) {
          throw error;
        }
        //console.log('inserted new')
        res.redirect('/')    
      });
    //res.json(`Updated Yoda's quote`)

    };
  });
});

// app.delete('/quotes', (req, res) => {
//   const nameToDelete = 'Darth Vader';
//   pool.query("DELETE FROM public.quotes WHERE name IN (SELECT name FROM public.quotes WHERE name = $1 ORDER BY id LIMIT 1)", 
//   [nameToDelete], (error, result) => {
//     if (error) {
//       throw error;
//     }
//     //console.log(`Deleted ${result.rowCount} quote(s)`)
//     res.redirect('/');
//   });
// });
  
app.delete('/quotes', (req, res) => {
  const {name}= req.body;
  pool.query('SELECT id FROM public.quotes WHERE name = $1', [name], (error, result) => {
    if (error) {
      throw error;
    }

    const idsToDelete = result.rows.map(row => row.id);
    if (idsToDelete.length === 0) {
      res.json('No quote to delete')
      return ; 
    }

    const idToDelete = idsToDelete[0];
    pool.query('DELETE FROM public.quotes WHERE id = $1', [idToDelete], (error, result) => {
      if (error) {
        throw error;
      }
      res.redirect('/');
    });
  });
});



    

 



