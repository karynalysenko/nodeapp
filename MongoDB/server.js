const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const MongoClient = require('mongodb').MongoClient
app.use(express.static('public'))
app.set('view engine', 'ejs')
var favicon = require('serve-favicon');
app.use(bodyParser.json())


//app.use(favicon(__dirname + '/public/images/favicon.ico'));
var path = require('path')
//app.use(favicon(path.join(__dirname,'public','images','favicon.ico')));
app.use('/favicon.ico', express.static('images/favicon.ico'));



//Important 
app.use(bodyParser.urlencoded({ extended: true }))



// app.get('/', (req, res) => {
//   //res.send('Hello World!')
//   res.sendFile(__dirname + '/index.html')
// })

console.log(__dirname)

const url = 'mongodb://localhost:27017/star-wars-quotes';
MongoClient.connect(url, { useUnifiedTopology: true })
  .then(client => {
    console.log('Connected to Database')
    const db = client.db('star-wars-quotes')
    const quotesCollection = db.collection('quotes')


    app.listen(3000, function () {
      console.log('listening on 3000')
    })

    app.put('/quotes', (req, res) => {
      quotesCollection
      .findOneAndUpdate(
        { name: 'Yoda' },
        {
          $set: {
            name: req.body.name,
            quote: req.body.quote,
          },
        },
        {
          upsert: true,
        }
      )
      .then(result => {
        console.log("updated")
        res.json('update Success')

      })
      .catch(error => console.error(error))
      //console.log(req.body)
    })

    app.delete('/quotes', (req, res) => {
      quotesCollection
        .deleteOne({ name: req.body.name })
        .then(result => {
          if (result.deletedCount === 0) {
            return res.json('No quote to delete')
          }
          console.log("deleted")
          res.json(`Deleted Darth Vader's quote`)

        })
        .catch(error => console.error(error))
    })

    app.post('/quotes', (req, res) => {
      console.log("vai inserir")
      console.log(req.body)

      quotesCollection
        .insertOne(req.body)
        .then(result => {
          res.redirect('/')
        })
        .catch(error => console.error(error))
    })  
    app.get('/', (req, res) => {
      quotesCollection
        .find()
        .toArray()
        .then(results => {
          res.render('index.ejs', { quotes: results })
          //console.log(results)
        })
        .catch(error => console.error(error))
      })
    
  })
  .catch(error => {
    console.error('Error connecting to database:', error);
  });


